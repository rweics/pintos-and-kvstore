package kvstore;

import static kvstore.KVConstants.*;

import java.io.IOException;
import java.net.Socket;

/**
 * This NetworkHandler will asynchronously handle the socket connections.
 * It uses a threadPool to ensure that none of it's methods are blocking.
 */
public class TPCClientHandler implements NetworkHandler {

    public TPCMaster tpcMaster;
    public ThreadPool threadPool;

    /**
     * Constructs a TPCClientHandler with ThreadPool of a single thread.
     *
     * @param tpcMaster TPCMaster to carry out requests
     */
    public TPCClientHandler(TPCMaster tpcMaster) {
        this(tpcMaster, 1);
    }

    /**
     * Constructs a TPCClientHandler with ThreadPool of a single thread.
     *
     * @param tpcMaster TPCMaster to carry out requests
     * @param connections number of threads in threadPool to service requests
     */
    public TPCClientHandler(TPCMaster tpcMaster, int connections) {
        // implemented by xia
    	this.threadPool = new ThreadPool(connections);
        this.tpcMaster = tpcMaster;
    }

    /**
     * Creates a job to service the request on a socket and enqueues that job
     * in the thread pool. Ignore InterruptedExceptions.
     *
     * @param client Socket connected to the client with the request
     */
    @Override
    public void handle(Socket client) {
        // implemented by xia
    	try{
        	this.threadPool.addJob(new ClientRunnable(client));
        } catch(Exception e){
        	//
        }
    }
    
    // implement by xia
    private class ClientRunnable implements Runnable{
    	private Socket client;
    	public ClientRunnable(Socket client){
    		this.client = client;
    	}
    	
		@Override
		public void run() {
			KVMessage respond = null;
			try{
				KVMessage clientMsg = new KVMessage(this.client);
				String clientMsgType = clientMsg.getMsgType();
				String key = clientMsg.getKey();
				if(key == null || key.length() == 0)
					throw new KVException(KVConstants.ERROR_INVALID_KEY);
				switch(clientMsgType){
					case KVConstants.PUT_REQ:
						tpcMaster.handleTPCRequest(clientMsg, true);
						respond = new KVMessage(KVConstants.RESP,KVConstants.SUCCESS);
						break;
					case KVConstants.DEL_REQ:
						tpcMaster.handleTPCRequest(clientMsg, false);
						respond = new KVMessage(KVConstants.RESP,KVConstants.SUCCESS);
						break;
					case KVConstants.GET_REQ:
						respond = new KVMessage(KVConstants.RESP);
						respond.setKey(key);
						respond.setValue(tpcMaster.handleGet(clientMsg));
						break;
				}
			}catch(KVException e){
				respond = new KVMessage(e.getKVMessage());
			}
			try{
				respond.sendMessage(this.client);
			} catch(KVException e){
				//
			}
			
		}
    }
}
