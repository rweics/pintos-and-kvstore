package kvstore;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadPool {

    /* Array of threads in the threadpool */
    public Thread threads[];
    /* Roy */
    private ReentrantLock lock;
    private ArrayList<Runnable> jobs;
    private Condition is_empty;
    /* Roy */
    /**
     * Constructs a Threadpool with a certain number of threads.
     *
     * @param size number of threads in the thread pool
     */
    public ThreadPool(int size) {
    	/* Roy */
        this.threads = new Thread[size];
        this.jobs = new ArrayList<Runnable>();
        //this.lock = new ReentrantLock();
        //this.is_empty = new ReentrantLock();
        this.lock = new ReentrantLock();
        this.is_empty = lock.newCondition();
        
        
        for(int i = 0; i < size; i++){
        	threads[i] = this.new WorkerThread(this);
        	threads[i].start();
        }
        /* end */
        // implement me
    }

    /**
     * Add a job to the queue of jobs that have to be executed. As soon as a
     * thread is available, the thread will retrieve a job from this queue if
     * if one exists and start processing it.
     *
     * @param r job that has to be executed
     * @throws InterruptedException if thread is interrupted while in blocked
     *         state. Your implementation may or may not actually throw this.
     */
    public void addJob(Runnable r) throws InterruptedException {
        // implement me
    	/* Roy */
    	//synchronized(this){
	    	this.lock.lock();
    		this.jobs.add(r);
	    	this.is_empty.signal();
	    	this.lock.unlock();
    	//}
	    /* Roy */
    }

    /**
     * Block until a job is present in the queue and retrieve the job
     * @return A runnable task that has to be executed
     * @throws InterruptedException if thread is interrupted while in blocked
     *         state. Your implementation may or may not actually throw this.
     */
    public Runnable getJob() throws InterruptedException {
        // implement me
    	/* Roy */
    	//synchronized(this){
    		this.lock.lock();
	    	Runnable job;
	    	while(this.jobs.isEmpty())
	    		this.is_empty.await();
	    	job = this.jobs.remove(0);
	        this.lock.unlock();
	        return job;
	    //}
        /* Roy */
    }

    /**
     * A thread in the thread pool.
     */
    public class WorkerThread extends Thread {

        public ThreadPool threadPool;

        /**
         * Constructs a thread for this particular ThreadPool.
         *
         * @param pool the ThreadPool containing this thread
         */
        public WorkerThread(ThreadPool pool) {
            threadPool = pool;
        }

        /**
         * Scan for and execute tasks.
         */
        @Override
        public void run() {
            /* Roy */
        	while(true){
        		try {
					Runnable job = threadPool.getJob();
					job.run();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					System.err.println("interrupt exception in ThreadPool!");
				}
        		
        	}
        	/* Roy */
        }
    }
}
