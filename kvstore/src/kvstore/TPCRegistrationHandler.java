package kvstore;

import static kvstore.KVConstants.*;

import java.io.IOException;
import java.net.Socket;

/**
 * This NetworkHandler will asynchronously handle the socket connections.
 * Uses a thread pool to ensure that none of its methods are blocking.
 */
public class TPCRegistrationHandler implements NetworkHandler {

    private ThreadPool threadpool;
    private TPCMaster master;

    /**
     * Constructs a TPCRegistrationHandler with a ThreadPool of a single thread.
     *
     * @param master TPCMaster to register slave with
     */
    public TPCRegistrationHandler(TPCMaster master) {
        this(master, 1);
    }

    /**
     * Constructs a TPCRegistrationHandler with ThreadPool of thread equal to the
     * number given as connections.
     *
     * @param master TPCMaster to carry out requests
     * @param connections number of threads in threadPool to service requests
     */
    public TPCRegistrationHandler(TPCMaster master, int connections) {
        this.threadpool = new ThreadPool(connections);
        this.master = master;
    }

    /**
     * Creates a job to service the request on a socket and enqueues that job
     * in the thread pool. Ignore any InterruptedExceptions.
     *
     * @param slave Socket connected to the slave with the request
     */
    @Override
    public void handle(Socket slave) {
        // implemented by xia
        try{
        	this.threadpool.addJob(new RegistrationRunnable(slave));
        } catch(Exception e){
        	//
        }
    }

    // implemented by xia
	/**
	 *  Registering using TPCMaster by parsing information from the passed in slave request.
	 */
    private class RegistrationRunnable implements Runnable{

    	private Socket slave;
    	
		public RegistrationRunnable(Socket slave) {
			this.slave = slave;
		}
		
		@Override
		public void run() {
			KVMessage respond = null;
			try{
				KVMessage slaveMsg = new KVMessage(this.slave);
				TPCSlaveInfo slaveServer = new TPCSlaveInfo(slaveMsg.getMessage());
				master.registerSlave(slaveServer);
				respond = new KVMessage(KVConstants.RESP,"Successfully registered " + slaveServer.toString());
			}catch(Exception e){
				respond = new KVMessage(KVConstants.ERROR_INVALID_FORMAT);
			}
			try{
				respond.sendMessage(this.slave);
			}catch(KVException e){
				//
			}
		}
    	
    }
}
