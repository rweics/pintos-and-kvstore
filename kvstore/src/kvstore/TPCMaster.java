package kvstore;

import static kvstore.KVConstants.*;

import java.net.Socket;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TPCMaster {

	public int numSlaves;
	public KVCache masterCache;
	volatile private KVMessage error;
	private Lock reqLock;
	/* Since Hashtable is synchronized, we do not need a lock for registration */
	private Hashtable<Long, TPCSlaveInfo> registeredSlaves;
	private ArrayList<Long> slaveIDs;

	public static final int TIMEOUT = 3000;

	/**
	 * Creates TPCMaster, expecting numSlaves slave servers to eventually register
	 *
	 * @param numSlaves number of slave servers expected to register
	 * @param cache KVCache to cache results on master
	 */
	public TPCMaster(int numSlaves, KVCache cache) {
		this.numSlaves = numSlaves;
		this.masterCache = cache;
		this.reqLock = new ReentrantLock();
		// implement me
		this.registeredSlaves = new Hashtable<Long, TPCSlaveInfo>();
		this.slaveIDs = new ArrayList<Long>();
	}


	/**
	 * Registers a slave. Drop registration request if numSlaves already
	 * registered. Note that a slave re-registers under the same slaveID when
	 * it comes back online.
	 *
	 * @param slave the slaveInfo to be registered
	 */
	public void registerSlave(TPCSlaveInfo slave) {
		// implement me
		long slaveID = slave.getSlaveID();
		/* Add slave to registeredSlaves if the slave already exist or there is still capacity to add more */
		if (this.registeredSlaves.containsKey(slaveID) || getNumRegisteredSlaves() < this.numSlaves) {
			this.registeredSlaves.put(slaveID, slave);
			this.slaveIDs.add(slaveID);
		}
	}

	/**
	 * Converts Strings to 64-bit longs. Borrowed from http://goo.gl/le1o0W,
	 * adapted from String.hashCode().
	 *
	 * @param string String to hash to 64-bit
	 * @return long hashcode
	 */
	public static long hashTo64bit(String string) {
		long h = 1125899906842597L;
		int len = string.length();

		for (int i = 0; i < len; i++) {
			h = (31 * h) + string.charAt(i);
		}
		return h;
	}

	/**
	 * Compares two longs as if they were unsigned (Java doesn't have unsigned
	 * data types except for char). Borrowed from http://goo.gl/QyuI0V
	 *
	 * @param n1 First long
	 * @param n2 Second long
	 * @return is unsigned n1 less than unsigned n2
	 */
	public static boolean isLessThanUnsigned(long n1, long n2) {
		return (n1 < n2) ^ ((n1 < 0) != (n2 < 0));
	}

	/**
	 * Compares two longs as if they were unsigned, uses isLessThanUnsigned
	 *
	 * @param n1 First long
	 * @param n2 Second long
	 * @return is unsigned n1 less than or equal to unsigned n2
	 */
	public static boolean isLessThanEqualUnsigned(long n1, long n2) {
		return isLessThanUnsigned(n1, n2) || (n1 == n2);
	}

	/**
	 * Find primary replica for a given key.
	 *
	 * @param key String to map to a slave server replica
	 * @return SlaveInfo of first replica
	 */
	public TPCSlaveInfo findFirstReplica(String key) {
		// implement me
		Collections.sort(this.slaveIDs);
		int index = Collections.binarySearch(slaveIDs, hashTo64bit(key));
		if (index < 0) {
			index = -1 * (index + 1) ;
			if (index == this.slaveIDs.size()) {
				index = 0;
			}
		}
		return this.registeredSlaves.get(this.slaveIDs.get(index));
	}

	/**
	 * Find the successor of firstReplica.
	 *
	 * @param firstReplica SlaveInfo of primary replica
	 * @return SlaveInfo of successor replica
	 */
	public TPCSlaveInfo findSuccessor(TPCSlaveInfo firstReplica) {
		// implement me
		Collections.sort(this.slaveIDs);
		int index = Collections.binarySearch(slaveIDs, firstReplica.getSlaveID()) + 1;
		if (index >= registeredSlaves.size())
			index = 0;
		if (index >= 0)
			return this.registeredSlaves.get(this.slaveIDs.get(index));
		else{
			return this.registeredSlaves.get(this.slaveIDs.get(registeredSlaves.size()-1));
		}
	}

	/**
	 * @return The number of slaves currently registered.
	 */
	public int getNumRegisteredSlaves() {
		// implement me
		return registeredSlaves.size();
	}

	/**
	 * (For testing only) Attempt to get a registered slave's info by ID.
	 * @return The requested TPCSlaveInfo if present, otherwise null.
	 */
	public TPCSlaveInfo getSlave(long slaveId) {
		// implement me
		return registeredSlaves.get(slaveId);
	}

	/**
	 * Perform 2PC operations from the master node perspective. This method
	 * contains the bulk of the two-phase commit logic. It performs phase 1
	 * and phase 2 with appropriate timeouts and retries.
	 *
	 * See the spec for details on the expected behavior.
	 *
	 * @param msg KVMessage corresponding to the transaction for this TPC request
	 * @param isPutReq boolean to distinguish put and del requests
	 * @throws KVException if the operation cannot be carried out for any reason
	 */
	public synchronized void handleTPCRequest(KVMessage msg, boolean isPutReq)
			throws KVException {
		// implement me
		String error = null;
		PhaseOne Phase11 = null;
		PhaseOne Phase12 = null;
		PhaseTwo Phase21 = null;
		PhaseTwo Phase22 = null;
		TPCSlaveInfo successor = null;
		reqLock = masterCache.getLock(msg.getKey());
		reqLock.lock();
		String msgKey = msg.getKey();
		successor = findSuccessor(findFirstReplica(msgKey));
		Phase11 = new PhaseOne(findFirstReplica(msgKey), msg);
		Phase12 = new PhaseOne(successor, msg);
		try{
			Phase11.run();
			Phase12.run();	
		} catch (KVException e){
			error = e.getMessage();
		}
		
		boolean err = error == null;
		Phase21 = new PhaseTwo(msgKey, true, err);
		Phase22 = new PhaseTwo(msgKey, false, err);
		try{
			Phase21.run();
		} catch (KVException e){
			error = e.getMessage();
		}
		try{
			Phase22.run();
		}catch (KVException e){
			error = e.getMessage();
		}
		if (error != null){
			reqLock.unlock();
			throw new KVException(error);
		}
		if (isPutReq)
			masterCache.put(msgKey, msg.getValue());
		else
			masterCache.del(msgKey);
		reqLock.unlock();
	}

	/**
	 * Perform GET operation in the following manner:
	 * - Try to GET from cache, return immediately if found
	 * - Try to GET from first/primary replica
	 * - If primary succeeded, return value
	 * - If primary failed, try to GET from the other replica
	 * - If secondary succeeded, return value
	 * - If secondary failed, return KVExceptions from both replicas
	 *
	 * @param msg KVMessage containing key to get
	 * @return value corresponding to the Key
	 * @throws KVException with ERROR_NO_SUCH_KEY if unable to get
	 *         the value from either slave for any reason
	 */
	public String handleGet(KVMessage msg) throws KVException {
		String value = null;
		String message = null;
		KVMessage response = null;
		Socket prim_rep_sock = null;
		Socket second_rep_sock = null;
		TPCSlaveInfo secondRep = null;
		TPCSlaveInfo primaryRep = null;
		try{
			String key = msg.getKey();
			reqLock = this.masterCache.getLock(key);
			reqLock.lock();
			String val = this.masterCache.get(key);
			if (val != null)
				return val;
			KVMessage request = new KVMessage(KVConstants.GET_REQ);
			/* search in the primary replica */
			primaryRep = this.findFirstReplica(key);
			prim_rep_sock = primaryRep.connectHost(TIMEOUT);
			request.sendMessage(prim_rep_sock);
			/* search in secondary replica */
			secondRep = this.findSuccessor(primaryRep);

			//msg.sendMessage(prim_rep_sock);
			response = new KVMessage(prim_rep_sock, 0);
			primaryRep.closeHost(prim_rep_sock);
			message = response.getMessage();
			if (message == null)
				value = response.getValue();
			if (value == null){
				second_rep_sock = secondRep.connectHost(TIMEOUT);
				msg.sendMessage(second_rep_sock);
				response = new KVMessage(second_rep_sock, 0);
				secondRep.closeHost(second_rep_sock);
				message = response.getMessage();
				if (message == null)
					value = response.getValue();
				else
					throw new KVException(message);
			}
			if(value != null){
				this.masterCache.put(key, value);
				secondRep.closeHost(second_rep_sock);
				return value;
			}
			secondRep.closeHost(second_rep_sock);
			reqLock.unlock();
			throw new KVException(KVConstants.ERROR_NO_SUCH_KEY);
		}catch (KVException ex){
			throw ex;
		}
	}
	private class PhaseOne{

		private TPCSlaveInfo slaveInfo;
		private KVMessage msg;

		public PhaseOne(TPCSlaveInfo slaveInfo, KVMessage msg){
			this.slaveInfo = slaveInfo;
			this.msg = msg;
		}

		public void run() throws KVException{
			Socket sock = null;
			KVMessage response = null;
			sock = slaveInfo.connectHost(TIMEOUT);
			msg.sendMessage(sock);
			response = new KVMessage(sock, TIMEOUT);
			slaveInfo.closeHost(sock);
			if (response.getMsgType().equals(KVConstants.ABORT)){
				throw new KVException(response);
			}
			
		}

	}

	private class PhaseTwo
	{

		private String key;
		private boolean isCommit;
		private boolean isPrimarySlave;
		public PhaseTwo(String key, boolean isPrimarySlave, boolean CommitBool){
			this.key = key;
			this.isPrimarySlave = isPrimarySlave;
			this.isCommit = CommitBool;
		}

		public void run() throws KVException{
			KVMessage msg = null;
			KVMessage response = null;
			TPCSlaveInfo slaveInfo = null;
			if (isCommit)
				msg = new KVMessage(KVConstants.COMMIT);
			else
				msg = new KVMessage(KVConstants.ABORT);
			
			while (true){
				if (isPrimarySlave){
					slaveInfo = findFirstReplica(key); 
				}else{
					slaveInfo = findSuccessor(findFirstReplica(key));
				}
				try{
					Socket sock = slaveInfo.connectHost(TIMEOUT);
					msg.sendMessage(sock);
					response = new KVMessage(sock, TIMEOUT);
					slaveInfo.closeHost(sock);
				} catch (KVException e){
					if (e.getMessage() == KVConstants.ERROR_SOCKET_TIMEOUT ||
						e.getMessage() == KVConstants.ERROR_COULD_NOT_CONNECT){
						continue;
					} else {
						throw e;
					}
				}
				if (response.getMsgType().equals(KVConstants.ACK)){
					return;	
				} else{
					throw new KVException(KVConstants.ERROR_INVALID_FORMAT);
				}
			}
		}
	}
}

