package kvstore;

import static kvstore.KVConstants.*;

import java.io.IOException;
import java.net.Socket;
/**
 * Implements NetworkHandler to handle 2PC operation requests from the Master/
 * Coordinator Server
 */
public class TPCMasterHandler implements NetworkHandler {

	public long slaveID;
	public TPCLog tpcLog;
	public KVServer KvServer;
	public ThreadPool threadpool;

	// implement me

	/**
	 * Constructs a TPCMasterHandler with one connection in its ThreadPool
	 *
	 * @param slaveID the ID for this slave server
	 * @param kvServer KVServer for this slave
	 * @param log the log for this slave
	 */
	public TPCMasterHandler(long slaveID, KVServer kvServer, TPCLog log) {
		this(slaveID, kvServer, log, 1);
	}

	/**
	 * Constructs a TPCMasterHandler with a variable number of connections
	 * in its ThreadPool
	 *
	 * @param slaveID the ID for this slave server
	 * @param kvServer KVServer for this slave
	 * @param log the log for this slave
	 * @param connections the number of connections in this slave's ThreadPool
	 */
	public TPCMasterHandler(long slaveID, KVServer kvServer, TPCLog log, int connections) {
		this.slaveID = slaveID;
		this.KvServer = kvServer;
		this.tpcLog = log;
		this.threadpool = new ThreadPool(connections);
	}

	/**
	 * Registers this slave server with the master.
	 *
	 * @param masterHostname
	 * @param server SocketServer used by this slave server (which contains the
	 *               hostname and port this slave is listening for requests on
	 * @throws KVException with ERROR_INVALID_FORMAT if the response from the
	 *         master is received and parsed but does not correspond to a
	 *         success as defined in the spec OR any other KVException such
	 *         as those expected in KVClient in project 3 if unable to receive
	 *         and/or parse message
	 */
	public void registerWithMaster(String masterHostname, SocketServer server)
			throws KVException {
		// implement me. xia
		Socket sock = null;
		KVMessage request = null;
		String message = null;
		try {
			message = slaveID + "@" + server.getHostname() + ":" + server.getPort();
			sock = new Socket(masterHostname, 9090);
			request = new KVMessage(KVConstants.REGISTER, message);
			request.sendMessage(sock);
			sock.close();
		}catch(IOException e) {
			throw new KVException(KVConstants.ERROR_COULD_NOT_CONNECT);
		}catch(Exception e){
			throw new KVException(KVConstants.ERROR_COULD_NOT_CREATE_SOCKET);
		}
	}

	/**
	 * Creates a job to service the request on a socket and enqueues that job
	 * in the thread pool. Ignore any InterruptedExceptions.
	 *
	 * @param master Socket connected to the master with the request
	 */
	@Override
	public void handle(Socket master) {
		// implement me. xia
		try{
			threadpool.addJob(new MasterRunnable(master,KvServer, tpcLog, ""));
		}catch(Exception e){
			//
		}
	}
	private class MasterRunnable implements Runnable{
		private TPCLog tpcLog;
		private KVServer kvServer;
		private Socket master;
		private String ax;
		public MasterRunnable(Socket master, KVServer kvServer, TPCLog tpcLog, String ax){
			this.kvServer = kvServer;
			this.tpcLog = tpcLog;
			this.master = master;
			this.ax = ax;
		}
		@Override
		public void run() {
			KVMessage message = null;
			try {
				message = new KVMessage(master);
			} catch (KVException e1) {
				e1.printStackTrace();
			}
			try{
				String type = message.getMsgType();
				switch(type){
				case KVConstants.GET_REQ:
					getHandler(message);
					break;
				case KVConstants.DEL_REQ:
					delHandler(message);
					break;
				case KVConstants.PUT_REQ:
					putHandler(message);
					break;
				case KVConstants.COMMIT:
					commitHandler(message);
					break;
				case KVConstants.ABORT:
					sendMessage(KVConstants.ACK, null);
					break;
				}
			}catch(KVException e){
				//
			}

		}
		private void getHandler(KVMessage message) throws KVException {
			String key = message.getKey();
			KVMessage response = null;
			try {
				response = new KVMessage(KVConstants.RESP);
				response.setKey(key);
				response.setValue(kvServer.get(key));
				response.sendMessage(master);
			} catch (KVException e) {
				sendMessage(KVConstants.RESP, e.getMessage());
			}

		}
		private void delHandler(KVMessage msg) throws KVException {
			try {
				this.tpcLog.appendAndFlush(msg);
				this.kvServer.get(msg.getKey());
				sendMessage(KVConstants.READY, null);
			} catch (KVException e) {
				sendMessage(KVConstants.ABORT, e.getMessage());
			}
		}

		private void putHandler(KVMessage msg) throws KVException {
			int maxKeySize = 256;
			int maxValueSize = 256 * 1024;
			String key, value;
			try {
				key = msg.getKey();
				value = msg.getValue();
				if (key == null || key.isEmpty())
					throw new KVException(KVConstants.ERROR_INVALID_KEY);
				else if (key.length() > maxKeySize)
					throw new KVException(KVConstants.ERROR_OVERSIZED_KEY);
				if (value == null || value.isEmpty())
					throw new KVException(KVConstants.ERROR_INVALID_VALUE);
				else if (value.length() > maxValueSize)
					throw new KVException(KVConstants.ERROR_OVERSIZED_VALUE);
			} catch (KVException e) {
				sendMessage(KVConstants.ABORT, e.getMessage()+ax);
			}
			this.tpcLog.appendAndFlush(msg);
			sendMessage(KVConstants.READY, null);
		}
		
		private void commitHandler(KVMessage msg) throws KVException {
			KVMessage lastMsg = null;
			String key, value;
			try{
				lastMsg = this.tpcLog.getLastEntry();
				key = lastMsg.getKey();
				value = lastMsg.getValue();
				switch (lastMsg.getMsgType()) {
				case KVConstants.DEL_REQ:
					this.tpcLog.appendAndFlush(msg);
					this.kvServer.del(key);
					break;
				case KVConstants.PUT_REQ:
					this.tpcLog.appendAndFlush(msg);
					this.kvServer.put(key, value);
					break;
				}
				sendMessage(KVConstants.ACK, null);
			}catch(KVException e){
				//
			}
		}

		private void sendMessage(String respType, String respMsg) throws KVException {
			KVMessage message = new KVMessage(respType);
			try{
				if (respMsg != null && !respMsg.isEmpty())
					message.setMessage(respMsg);
				if (respType.equals(KVConstants.ACK))
					tpcLog.appendAndFlush(message);
				message.sendMessage(master);
			}catch(KVException e){
				//
			}
		}

	}
}
