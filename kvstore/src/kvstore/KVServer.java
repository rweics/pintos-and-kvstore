package kvstore;

import static kvstore.KVConstants.ERROR_OVERSIZED_KEY;
import static kvstore.KVConstants.ERROR_OVERSIZED_VALUE;
import static kvstore.KVConstants.RESP;

import java.util.concurrent.locks.Lock;

/**
 * This class services all storage logic for an individual key-value server.
 * All KVServer request on keys from different sets must be parallel while
 * requests on keys from the same set should be serial. A write-through
 * policy should be followed when a put request is made.
 */
public class KVServer implements KeyValueInterface {

	private KVStore dataStore;
	private KVCache dataCache;

	private static final int MAX_KEY_SIZE = 256;
	private static final int MAX_VAL_SIZE = 256 * 1024;

	/**
	 * Constructs a KVServer backed by a KVCache and KVStore.
	 *
	 * @param numSets the number of sets in the data cache
	 * @param maxElemsPerSet the size of each set in the data cache
	 */

	public KVServer(int numSets, int maxElemsPerSet) {
		this.dataCache = new KVCache(numSets, maxElemsPerSet);
		this.dataStore = new KVStore();
	}

	/**
	 * Performs put request on cache and store.
	 *
	 * @param  key String key
	 * @param  value String value
	 * @throws KVException if key or value is too long
	 */
	@Override
	public void put(String key, String value) throws KVException {
		// implement by xia
		validate(key, "key", KVConstants.PUT_REQ);
		validate(value, "value", KVConstants.PUT_REQ);
		
		Lock lockPut = dataCache.getLock(key);
		lockPut.lock();
		dataCache.put(key, value);
		dataStore.put(key, value);
		lockPut.unlock();
	}

	/**
	 * Performs get request.
	 * Checks cache first. Updates cache if not in cache but located in store.
	 *
	 * @param  key String key
	 * @return String value associated with key
	 * @throws KVException with ERROR_NO_SUCH_KEY if key does not exist in store
	 */
	@Override
	public String get(String key) throws KVException {
		// implement by xia
		validate(key, "key", KVConstants.GET_REQ);
		Lock lockGet = dataCache.getLock(key);
		lockGet.lock();
		String val = null;
		try{
			val = dataCache.get(key);
			if (val != null) {
				lockGet.unlock();
			}else{
				val = dataStore.get(key);
				dataCache.put(key, val);
				lockGet.unlock();
			}
		}catch(Exception e){
			lockGet.unlock();
			throw new KVException(KVConstants.ERROR_NO_SUCH_KEY);
		}
		validate(val, "value", KVConstants.GET_REQ);
		return val;
	}

	/**
	 * Performs del request.
	 *
	 * @param  key String key
	 * @throws KVException with ERROR_NO_SUCH_KEY if key does not exist in store
	 */
	@Override
	public void del(String key) throws KVException {
		// implement by xia
		validate(key, "key", KVConstants.DEL_REQ);
		Lock lockDel = dataCache.getLock(key);
		lockDel.lock();
		dataCache.del(key);
		try {
			dataStore.del(key);
			lockDel.unlock();
		}
		catch(Exception e)
		{
			lockDel.unlock();
			throw new KVException(KVConstants.ERROR_NO_SUCH_KEY);
		}
	}

	/**
	 * Check if the server has a given key. This is used for TPC operations
	 * that need to check whether or not a transaction can be performed but
	 * you don't want to modify the state of the cache by calling get(). You
	 * are allowed to call dataStore.get() for this method.
	 *
	 * @param key key to check for membership in store
	 */
	public boolean hasKey(String key) {
		// implement by xia
		try{
			dataStore.get(key);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	// implement by xia
	private static void validate(String key, String type, String req) throws KVException{
		if(type.equals("key")){
			if (key == null || key.equals("")) 
				throw new KVException(KVConstants.ERROR_INVALID_KEY);
			else if (key.length() > MAX_KEY_SIZE)
				throw new KVException(new KVMessage(req, KVConstants.ERROR_OVERSIZED_KEY));
		}else{ // String key is type of value
			if (key == null || key.equals("")) 
				throw new KVException(KVConstants.ERROR_INVALID_VALUE);
			else if (key.length() > MAX_VAL_SIZE)
				throw new KVException(new KVMessage(req, KVConstants.ERROR_OVERSIZED_VALUE));
		}
		
	}
	/** This method is purely for convenience and will not be tested. */
	@Override
	public String toString() {
		return dataStore.toString() + dataCache.toString();
	}

}
