package kvstore;

import static kvstore.KVConstants.*;

import java.io.IOException;
import java.net.*;
import java.util.regex.*;

/**
 * Data structure to maintain information about SlaveServers
 */
public class TPCSlaveInfo {

    public long slaveID;
    public String hostname;
    public int port;

    /**
     * Construct a TPCSlaveInfo to represent a slave server.
     *
     * @param info as "SlaveServerID@Hostname:Port"
     * @throws KVException ERROR_INVALID_FORMAT if info string is invalid
     */
    public TPCSlaveInfo(String info) throws KVException {
        // implemented Hye
    	if (info == null){
    		//Exception message may need to be changed
    		throw new KVException(new KVMessage(ERROR_INVALID_FORMAT));
    	}
    	
    	//Parse SlaveServerID
    	int indexOfAt = info.indexOf('@');
    	//There is no @ in info
    	if (indexOfAt == -1){
    		throw new KVException(new KVMessage(ERROR_INVALID_FORMAT));
    	}
    	if (indexOfAt == info.lastIndexOf('@')){
    		try{
    			String beforeAt = info.substring(0, indexOfAt);
    			this.slaveID = Long.parseLong(beforeAt);
    		} catch (NumberFormatException e){
    			//Exception message may need to be changed
    			throw new KVException(new KVMessage(ERROR_INVALID_FORMAT));
    		}
    	} else {
    		//Exception message may need to be changed
    		throw new KVException(new KVMessage(ERROR_INVALID_FORMAT));
    	}
    	
    	//Parse hostname
    	int indexOfColon = info.indexOf(':');
    	if (indexOfColon == -1){
    		throw new KVException(new KVMessage(ERROR_INVALID_FORMAT));
    	}
    	if (indexOfColon == info.lastIndexOf(':')){
    		this.hostname = info.substring(indexOfAt+1, indexOfColon);
    	} else {
    		//Exception message may need to be changed
    		throw new KVException(new KVMessage(ERROR_INVALID_FORMAT));
    	}
    	
    	//Parse port
    	String afterColon = info.substring(indexOfColon+1, info.length());
    	try{
    		this.port = Integer.parseInt(afterColon);
    	} catch (NumberFormatException e){
    		//Exception message may need to be changed
    		throw new KVException(new KVMessage(ERROR_INVALID_FORMAT));
    	}
    }

    public long getSlaveID() {
        return slaveID;
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    /**
     * Create and connect a socket within a certain timeout.
     *
     * @return Socket object connected to SlaveServer, with timeout set
     * @throws KVException ERROR_SOCKET_TIMEOUT, ERROR_COULD_NOT_CREATE_SOCKET,
     *         or ERROR_COULD_NOT_CONNECT
     */
    public Socket connectHost(int timeout) throws KVException {
        // implemented Hye
    	Socket sock = new Socket();
    	try {	
    		sock.connect(new InetSocketAddress(this.hostname, this.port), timeout);
    	} catch (SocketTimeoutException e){
    		throw new KVException(ERROR_SOCKET_TIMEOUT);
    	} catch (IllegalArgumentException e){
    		throw new KVException(ERROR_COULD_NOT_CREATE_SOCKET);
    	} catch (IOException e){
    		throw new KVException(ERROR_COULD_NOT_CONNECT);
    	}
        return sock;
    }

    /**
     * Closes a socket.
     * Best effort, ignores error since the response has already been received.
     *
     * @param sock Socket to be closed
     */
    public void closeHost(Socket sock) {
        // implemented Hye
    	try {
    		sock.close();
    	} catch (IOException e){
    		
    	}
    }
}
