package kvstore;

import static kvstore.KVConstants.DEL_REQ;
import static kvstore.KVConstants.GET_REQ;
import static kvstore.KVConstants.PUT_REQ;
import static kvstore.KVConstants.RESP;
import static kvstore.KVConstants.SUCCESS;

import java.io.IOException;
import java.net.Socket;

/**
 * This NetworkHandler will asynchronously handle the socket connections.
 * Uses a thread pool to ensure that none of its methods are blocking.
 */
public class ServerClientHandler implements NetworkHandler {

    public KVServer kvServer;
    public ThreadPool threadPool;

    /**
     * Constructs a ServerClientHandler with ThreadPool of a single thread.
     *
     * @param kvServer KVServer to carry out requests
     */
    public ServerClientHandler(KVServer kvServer) {
        this(kvServer, 1);
    }

    /**
     * Constructs a ServerClientHandler with ThreadPool of thread equal to
     * the number passed in as connections.
     *
     * @param kvServer KVServer to carry out requests
     * @param connections number of threads in threadPool to service requests
     */
    public ServerClientHandler(KVServer kvServer, int connections) {
        // implement me
    	this.kvServer = kvServer;
    	this.threadPool = new ThreadPool(connections);
    }

    /**
     * Creates a job to service the request for a socket and enqueues that job
     * in the thread pool. Ignore any InterruptedExceptions.
     *
     * @param client Socket connected to the client with the request
     */
    @Override
    public void handle(Socket client) {
        // implement me
    	/* Roy */
    	ClientRunnable client_runnable = new ClientRunnable(client);
    	try {
			this.threadPool.addJob(client_runnable);
		} catch (InterruptedException e) {
			System.err.println("interrupt exception in ServerClientHandler");
		}
    	/* Roy */
    }
    
    class ClientRunnable implements Runnable{
    	Socket client;
    	
    	ClientRunnable(Socket client){
    		this.client = client;
    	}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			KVMessage request_message;
			try {
				request_message = new KVMessage(this.client);
			} catch (KVException e) {
				// TODO Auto-generated catch block
				System.err.println("KVException in ServerClientHandler.ClientRunnable");
				return;
			}
			try {
				KVMessage response_message = new KVMessage(KVConstants.RESP);
				String key = request_message.getKey();
				String val;
				switch(request_message.getMsgType()){
					case KVConstants.GET_REQ:
						val = kvServer.get(key);
						response_message.setKey(key);
						response_message.setValue(val);
						break;
					case KVConstants.PUT_REQ:
						val = request_message.getValue();
						kvServer.put(key, val);
						response_message.setMessage(KVConstants.SUCCESS);
						break;
					case KVConstants.DEL_REQ:
						kvServer.del(key);
						response_message.setMessage(KVConstants.SUCCESS);
						break;
				}
				response_message.sendMessage(this.client);
			}
			catch (KVException e) {
				// TODO Auto-generated catch block
				System.out.println("KVException in ServerClientHandler");
			}
			
			
		}
    	
    }
    
    // implement me

}
