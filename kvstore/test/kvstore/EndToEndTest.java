package kvstore;

import static autograder.TestUtils.*;
import static kvstore.KVConstants.*;
import static kvstore.Utils.*;
import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Random;

import kvstore.Utils.ErrorLogger;
import kvstore.Utils.RandomString;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import autograder.AGCategories.AGTestDetails;
import autograder.AGCategories.AG_PROJ3_CODE;

public class EndToEndTest extends EndToEndTemplate {
	@Test
    public void putAndGet() throws KVException {
        client.put("Go", "Bears");
        assertEquals(client.get("Go"), "Bears");
    }
    @Test
    public void testSamePut() throws KVException {
        client.put("foo", "foo");
        assertEquals(client.get("foo"), "foo");
    }
    @Test
    public void testReplace() throws KVException {
        client.put("stan", "ford");
        client.put("stan", "furd");
        client.get("stan");
        assertEquals(client.get("stan"), "furd");
    }
    @Test
    public void testMultiPut() throws KVException {
    	for(int i = 1; i <= 12; i++) {
    		client.put("Go"+i,"Bears"+i);
    	}
    	for(int j = 1; j <= 12; j++) {
    		assertEquals(client.get("Go"+j), "Bears"+j);
    	}
    }
}
