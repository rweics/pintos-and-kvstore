package kvstore;

import static org.junit.Assert.assertEquals;
import static kvstore.KVConstants.*;
import static org.junit.Assert.fail;

import org.junit.Test;

public class TPCEndToEndTest extends TPCEndToEndTemplate {

    @Test
    public void simplePutTest() throws KVException {
        client.put("meow", "frog");
        assertEquals("value of meow should be frog", "frog", client.get("meow"));
    }


    @Test
    public void overwriteValueTest() throws KVException {
        client.put("bahhh", "one");
        client.put("bahhh", "two");
        assertEquals("value of bahhh should be two", "two", client.get("bahhh"));
    }
    
    @Test
    (expected=KVException.class)
    public void noSuchKeyTest() throws KVException{
    	client.put("woof", "duck");
    	client.get("moooo");
    }

    @Test
    (expected=KVException.class)
    public void invalidKeyPutTest() throws KVException {
        client.put("", "baaah");
    }
    
    @Test
    (expected=KVException.class)
    public void invalidKeyGetTest() throws KVException{
    	client.get("");
    }

    @Test
    (expected=KVException.class)
    public void delNonexistant() throws KVException{
    	client.del("bananaman");
    }


}
