package kvstore;

import static org.junit.Assert.*;
import static kvstore.KVConstants.*;
import static kvstore.KVCache.*;
import static kvstore.TPCSlaveInfo.*;

import java.net.Socket;

import org.junit.*;


public class MasterSelfTest{
	/* Thoughts:
	 * 1. Should there be exception thrown if there is less than two slaves registered?
	 */
	
    @Test
    public void noSameSlave() throws KVException{
    	//Does not register the same slave twice
    	TPCSlaveInfo s1 = new TPCSlaveInfo("123@meow:80");
    	TPCSlaveInfo s2 = new TPCSlaveInfo("456@hoho:90");
    	TPCMaster meow = new TPCMaster(3, new KVCache(4, 2));
    	meow.registerSlave(s1);
    	meow.registerSlave(s2);
    	meow.registerSlave(s1);
    	//Assumes getNumRegistered Slaves is correct
    	assertEquals("there should be only 2 slaves registered", 2, meow.getNumRegisteredSlaves());
    }
    
    @Test
    public void noMoreSlaveThanLimit() throws KVException{
    	//Does not register the same slave twice
    	TPCSlaveInfo s1 = new TPCSlaveInfo("123@meow:80");
    	TPCSlaveInfo s2 = new TPCSlaveInfo("456@hoho:90");
    	TPCMaster meow = new TPCMaster(1, new KVCache(4, 2));
    	meow.registerSlave(s1);
    	meow.registerSlave(s2);

    	//Assumes getNumRegistered Slaves is correct
    	assertEquals("there should be only 1 slave registered", 1, meow.getNumRegisteredSlaves());
    }
    
    @Test
    public void getTheRightSlave() throws KVException{
    	//Does not register the same slave twice
    	TPCSlaveInfo s1 = new TPCSlaveInfo("123@woof:80");
    	TPCSlaveInfo s2 = new TPCSlaveInfo("456@hoho:90");
    	TPCMaster meow = new TPCMaster(5, new KVCache(4, 2));
    	meow.registerSlave(s1);
    	meow.registerSlave(s2);

    	//Assumes getNumRegistered Slaves is correct
    	assertEquals("slaveID 123 should have hostname woof", "woof", meow.getSlave(123L).getHostname());
    	assertEquals("slaveID 123 should have portNum 80", 80, meow.getSlave(123L).getPort());
    }
    
    @Test
    public void getFirstReplicaSimpleTest() throws KVException{
    	TPCSlaveInfo s1 = new TPCSlaveInfo("123@woof:80");
    	TPCSlaveInfo s2 = new TPCSlaveInfo("456@hoho:90");
    	TPCMaster meow = new TPCMaster(5, new KVCache(4, 2));
    	meow.registerSlave(s1);
    	meow.registerSlave(s2);
    	
    	System.err.println(meow.hashTo64bit("haha"));
    	TPCSlaveInfo slave = meow.findFirstReplica("haha");
    	assertEquals("first rep should have hostname woof", "woof", slave.getHostname());
    	
    	TPCSlaveInfo rep = meow.findSuccessor(slave);
    	assertEquals("sec rep hostname hoho", "hoho", rep.getHostname());
    }
}