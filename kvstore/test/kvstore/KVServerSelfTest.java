package kvstore;

import static org.junit.Assert.*;
import static kvstore.KVConstants.*;
import static kvstore.KVCache.*;
import static kvstore.TPCSlaveInfo.*;
import static kvstore.KVServer.*;
import static kvstore.KVException.*;

import org.junit.*;



public class KVServerSelfTest {
	@Test
	public void basicPutGet() throws KVException{
		KVServer server = new KVServer(2,3);
		server.put("george", "yiu");
		assertEquals(server.get("george"), "yiu");
	}
	
	@Test
	(expected=KVException.class)
	public void invalidKey() throws KVException{
		KVServer server = new KVServer(2,3);
		server.put("", "frog");
	}
}
