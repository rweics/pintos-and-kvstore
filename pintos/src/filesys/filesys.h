#ifndef FILESYS_FILESYS_H
#define FILESYS_FILESYS_H

#include <stdbool.h>
#include <list.h>
#include "filesys/off_t.h"

/* Sectors of system file inodes. */
#define FREE_MAP_SECTOR 0       /* Free map file inode sector. */
#define ROOT_DIR_SECTOR 1       /* Root directory file inode sector. */

struct file_descriptor {
    int fd_id;                     // The file descriptor returned to user at open(), will be used for lookup
    struct file *file;             // The mapped file
    struct thread *process;        // The process requested to access the file
    off_t pos;                     // Current position
    struct list_elem procelem;     // List element (for <struct thread>)
    struct list_elem fileelem;     // List element (for <struct file>)
};

/* Block device that contains the file system. */
struct block *fs_device;

/* File descriptor number to use when allocating new file descriptor it keeps growing up. */
int new_fd_number;

void filesys_init (bool format);
void filesys_done (void);
bool filesys_create (const char *name, off_t initial_size);
struct file *filesys_open (const char *name);
bool filesys_remove (const char *name);

#endif /* filesys/filesys.h */
