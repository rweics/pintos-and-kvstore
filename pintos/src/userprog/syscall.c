
#include "userprog/syscall.h"
#include <stdio.h>
#include <list.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "devices/shutdown.h"
#include "userprog/pagedir.h"
#include "threads/vaddr.h"
#include "filesys/filesys.h"
#include "filesys/file.h"

static void syscall_handler (struct intr_frame *);

//===========SYS_CALLS==========//
void sys_halt(tok_t args[], struct intr_frame *f){
  shutdown_power_off();
}
void sys_exit(tok_t args[], struct intr_frame *f){
  f->eax = args[0];
  exit(args[0]);
}
void sys_null(tok_t args[], struct intr_frame *f){
  f->eax = args[0] + 1;
}
void sys_wait(tok_t args[], struct intr_frame *f){
//printf("in sys_wait.....\n");
  f->eax = wait(args[0]);
}
void sys_exec(tok_t args[], struct intr_frame *f){
  uint32_t* addr = args[0];
  if(!isValidAddress((uint32_t*)addr)){
    exit(-1);
  }
  //printf("in sys_exec ....\n");
  char *file_name = args[0];
    /* Need to validate pointer here*/
    tid_t pid = process_execute (file_name);
    //printf("pid=%d\n", pid);
    f->eax = pid;
    sema_down(&thread_current()->sema_syscall_exec);
    if (pid != TID_ERROR) {
      enum intr_level old_level;
      old_level = intr_disable();
      struct thread *child = thread_get_by_tid(pid);
      intr_set_level(old_level);
      if (!child) f->eax = -1;
      // else {
      //   list_push_back (&thread_current()->child_proc, &child->childelem);
      // }
    } else f->eax = -1;
}
void sys_create(tok_t args[], struct intr_frame *f){
  if (isValidAddress((uint32_t*) args[0])) {
    f->eax = filesys_create ((const char*) args[0], (off_t) args[1]);
  } else {
    f->eax = false;
  }
}
void sys_remove(tok_t args[], struct intr_frame *f){
  if (isValidAddress((uint32_t*) args[0])) {
    f->eax = filesys_remove ((const char*) args[0]);
  } else {
    f->eax = false;
  }
}
void sys_open(tok_t args[], struct intr_frame *f){
  if (isValidAddress((uint32_t*) args[0])) {
    struct file *file = filesys_open((const char*) args[0]);
    if (!file) {
      f->eax = -1;
    } else {
      struct file_descriptor *fd = calloc (1, sizeof *fd);
      fd->fd_id = new_fd_number;
      new_fd_number += 1;
      fd->pos = 0;
      fd->file = file;
      fd->process = thread_current();
      //  Add the file_descriptor to the fd list both under the process and the file 
      list_push_front(&file->file_descriptors, &fd->fileelem);
      list_push_front(&thread_current()->file_descriptors, &fd->procelem);
      return fd->fd_id;
    }
  } else {
    f->eax = -1;
  }
}
void sys_filesize(tok_t args[], struct intr_frame *f){

}
void sys_read(tok_t args[], struct intr_frame *f){

}
void sys_write(tok_t args[], struct intr_frame *f){
  printf("%s", (char*) args[1]);
}
void sys_seek(tok_t args[], struct intr_frame *f){

}
void sys_tell(tok_t args[], struct intr_frame *f){

}
void sys_close(tok_t args[], struct intr_frame *f){
  
}
//===========SYS_CALLS=========//

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

typedef void sys_fun(tok_t args[], struct intr_frame *f);
typedef struct fun_desc{
  sys_fun *fun;
  int syscall;
  char *doc;
} fun_desc;

//=========syscall table========//
fun_desc sys_table[] = {
    {sys_halt, SYS_HALT, "halt"},
    {sys_exit, SYS_EXIT, "exit"},
    {sys_null, SYS_NULL, "hull"},
    {sys_wait, SYS_WAIT, "wait"},
    {sys_exec, SYS_EXEC, "exec"},
    {sys_write, SYS_WRITE, "write"},
    {sys_create, SYS_CREATE, "create"},
    {sys_remove, SYS_REMOVE, "remove"},
    {sys_open, SYS_OPEN, "open"},
    {sys_filesize, SYS_FILESIZE, "filesize"},
    {sys_read, SYS_READ, "read"},
    {sys_seek, SYS_SEEK, "seek"},
    {sys_tell, SYS_TELL, "tell"},
    {sys_close, SYS_CLOSE, "close"}
};
//=========syscall table========//

static void
syscall_handler (struct intr_frame *f UNUSED) 
{

  //printf("%s\n", "Starting syscall_handler....");
  uint32_t* args = ((uint32_t*) f->esp);
  //printf("System call number: %d\n", args[0]);

  int fundex = -1;
  fundex = lookup(args[0]);
  if (fundex >= 0){
    sys_table[fundex].fun(&args[1], f);
  }
}
int wait (pid_t pid){
  //printf("in int wait(pid=%d) func\n", pid);
  return process_wait(pid);
}
void exit(int s){
  printf("%s: exit(%d)\n", thread_current()->name, s);
  thread_exit();
}
// lookup the index for each syscall in sys_table
int lookup(int sys_int) {
  int i;
  for (i=0; i < (sizeof(sys_table)/sizeof(fun_desc)); i++) {
    if (sys_table[i].syscall == sys_int) return i;
  }
  return -1;
}
int isValidAddress(uint32_t* addr){
  struct thread* thread_curr = thread_current();
  uint32_t* pd = thread_curr->pagedir;
  if(addr == NULL || is_kernel_vaddr(addr) || pagedir_get_page(pd, addr) == NULL){
    return 0;
  }
  return 1;
}