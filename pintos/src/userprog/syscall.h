#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include "userprog/parse.h"
void syscall_init (void);
int lookup(int sys_int);
typedef int pid_t;
#endif /* userprog/syscall.h */
